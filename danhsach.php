<!DOCTYPE html>
<html lang='en'>

<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Danh sách sinh viên</title>
    <style>
        .container {
            width: 800px;
            margin: 100px auto;
            padding: 20px;
            border: 1px solid #43729d;
            font-size: 20px;
        }

        .form {
            display: flex;
            flex-direction: column;
            width: 60%;
            min-width: 400px;
            align-items: stretch;
            margin: 0 auto;
        }

        select {
            width: 220px;
            background-color: #DDE5EF;
            border: 1px solid #43729d;
            font-size: 20px;
            padding-left: 10px;
        }

        label {
            display: flex;
            width: 100px;
            height: 30px;
            align-items: center;
            box-sizing: border-box;
        }

        .form_item {
            display: flex;
            justify-content: center;
            margin: 5px 0;
        }

        input {
            width: 200px;
            border: 1px solid #43729d;
            background-color: #DDE5EF;
            font-size: 20px;
            padding-left: 15px;
        }

        /* select#faculty {
        border: #4f81bd solid 2px;
        width: 205px;
        height: 35px;
        background-color: #e1eaf4 !important;
        font-size: 14px;
        } */

        .buttons {
            display: flex;
        }

        button {
            display: flex;
            justify-content: center;
            min-width: 100px;
            font-size: 18px;
            padding: 10px 20px;
            color: white;
            background-color: #4F80BC;
            border: 2px solid #42648F;
            border-radius: 10px;
        }

        .button {
            min-width: 10px;
            font-size: 18px;
            padding: 2px 15px;
            color: white;
            background-color: #8EADD2;
            border: 2px solid #7D99BB;
            border-radius: 0px;
            margin-right: 10px;
        }

        table {
            width: 100%;
            margin: 20px 0px;
        }

        th {
            text-align: start;
        }
    </style>
</head>

<?php
if (isset($_POST['search'])) {
    session_start();
    $_SESSION = $_POST;
}
?>

<body>
    <div class="container">
        <form action="danhsach.php" method="POST" id="myform">
            <div class="form">
                <?php
                $faculties = array('' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                ?>
                <div class="form_item">
                    <label>Khoa</label>
                    <select id="faculty" name="faculty">
                        <?php
                        foreach ($faculties as $key => $value) {
                            $chose = (isset($_SESSION['faculty']) && $key == $_SESSION['faculty']) ? "selected" : "";
                            echo '<option value=' . $key . ' ' . $chose . '>' . $value . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="form_item">
                    <label>Từ khóa</label>
                    <?php
                    $keyword = isset($_SESSION['keyword']) ? $_SESSION['keyword'] : "";
                    echo '<input type="text" class="form_item_input" id="keyword" name="keyword" value="' . $keyword . '">';
                    ?>
                </div>
                <div class="form_item">
                    <input name="delete" type="button" value="Xoá" onclick="reFresh()" style="margin-right: 30px;" class="cancel">
                    <button name="search" type="submit">Tìm kiếm</button>
                    <div class="form_item">
                    </div>
                </div>
            </div>
        </form>

        <script>
            function reFresh() {
                document.getElementById('faculty').value = '';
                document.getElementById('keyword').value = '';
                <?php
                if (isset($_SESSION)) {
                    unset($_SESSION);
                }
                if (isset($_POST)) {
                    unset($_POST);
                }
                ?>
            }
        </script>

        <?php
        $students = array(
            array(
                "name" => "Nguyễn Văn A",
                "faculty" => "MAT"
            ),
            array(
                "name" => "Trần Thị B",
                "faculty" => "MAT"
            ),
            array(
                "name" => "Nguyễn Hoàng C",
                "faculty" => "KDL"
            ),
            array(
                "name" => "Đinh Quang D",
                "faculty" => "KDL"
            )
        );
        $count = count($students);
        ?>
        <form action="dangky.php" style="margin-top: 20px;">
            <div class="information">
                <span><?php echo "Số sinh viên tìm thấy: " . $count ?></span>
            </div>
            <div class="form_item" style="display: flex; justify-content: flex-end">
                <button name="add" type="submit" style="display: flex; flex-direction: row-reverse; margin-right: 40px">Thêm</button>
            </div>
            <table>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
                <?php
                if ($count > 0) {
                    for ($i = 0; $i < $count; $i++) {
                        $student = $students[$i];
                        echo "<tr>";
                        echo "<td>" . $i + 1 . "</td>";
                        echo "<td>" . $student["name"] . "</td>";
                        echo "<td>" . $faculties[$student["faculty"]] . "</td>";
                        echo "<td width='20%'><div class='buttons'>
                            <button class='button'>Xóa</button>
                            <button class='button'>Sửa</button>
                        </div></td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<tr>";
                    echo "<td>&nbsp;</td>";
                    echo "<td>&nbsp;</td>";
                    echo "<td>&nbsp;</td>";
                    echo "<td>&nbsp;</td>";
                    echo "<td>&nbsp;</td>";
                    echo "<td>&nbsp;</td>";
                    echo "<td>&nbsp;</td>";
                    echo "</tr>";
                }
                ?>
            </table>
        </form>
    </div>
</body>

</html>