<!DOCTYPE html>
<html lang='en'>

<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Xác nhận</title>
    <style>
        form {
        width: 450px;
        height: 520px;
        border: solid #547fa5 2px;
        padding: 20px 40px 10px 20px;
        }

        table {
        border-collapse: separate;
        border-spacing: 15px 15px;
        }

        label {
        font-size: 14px;
        font-family: "Times New Roman", Times, serif;
        }

        .green_background {
        color: aliceblue;
        background-color: #70ad47;
        width: 80px;
        height: 30px;
        line-height: 30px;
        padding: 0px 10px;
        border: solid #41719c 2px;
        }

        .submit {
        background-color: #70ad47;
        color: aliceblue;
        justify-content: center;
        padding: 10px 20px 10px 20px;
        width: 30%;
        border-radius: 7px;
        font-family: "Times New Roman", Times, serif;
        font-size: 14px;
        border: solid #41719c 2px;
        margin: 5% 0% 0% 40%;
        }

    </style>
</head>

<body>
    <form>
        <?php
        session_start();
        ?>
        <table>
            <tr>
                <td>
                    <div class="green_background"><label> Họ và tên </label></div>
                </td>
                <td>
                    <lable><?php echo $_SESSION["name"]; ?></lable>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="green_background"><label> Giới tính </label></div>
                </td>
                <td>
                    <lable><?php echo $_SESSION["gender"]; ?></lable>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="green_background"><label> Phân khoa </label></div>
                </td>
                <td>
                    <lable><?php echo $_SESSION["faculty"]; ?></lable>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="green_background"><label> Ngày sinh </label></div>
                </td>
                <td>
                    <lable><?php echo $_SESSION["dateBirth"]; ?></lable>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="green_background"><label> Địa chỉ </label></div>
                </td>
                <td>
                    <lable><?php echo $_SESSION["address"]; ?></lable>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <div class="green_background"><label> Hình ảnh </label></div>
                </td>
                <td>
                    <img src='<?php echo $_SESSION["image"] ?>' width='90px' height="60px" />
                </td>
            </tr>
        </table>
        <input type='submit' name='submit' value='Xác nhận' class='submit'>
    </form>

</body>

</html>

